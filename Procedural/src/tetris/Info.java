package tetris;
import java.awt.Point;


public class Info {
	Point startCoordinates;
	Tetromino currentTetromino;
	Tetromino nextTetromino;
	int cellSize;
	int windowsHeight;
	int windowsWidth;
	long score;
	int level;
	int acceleration;
	boolean gameFinish;

	public Info() {
		super();
		this.startCoordinates = new Point(4, -2);
		this.cellSize = 80;
		this.windowsHeight = 1600;
		this.windowsWidth = 800;
		this.score = 0;
		this.currentTetromino = new Tetromino();
		this.nextTetromino = new Tetromino();
		this.level = 0;
		this.gameFinish = false;
	}
	
	public void newTetromino(int fullLines) {
		this.currentTetromino = this.nextTetromino;
		this.nextTetromino = new Tetromino();
		scoreAccordingToFullLine(fullLines);
		levelAccordingToScore();
	}
	
	private void scoreAccordingToFullLine(int fullLines) {
		switch (fullLines) {
			case 1: 
				this.score += (40 * (this.level + 1));
				break;
			case 2: 
				this.score += (100 * (this.level + 1));
				break;
			case 3: 
				this.score += (300 * (this.level + 1));
				break;
			case 4: 
				this.score += (1200 * (this.level + 1));
				break;
		}
	}
	
	private void levelAccordingToScore() {
		if (this.score >= 0 && this.score < 1000) {this.level = 0;}
		else if (this.score >= 1000 && this.score < 2000) {this.level = 1;}
		else if (this.score >= 2000 && this.score < 3000) {this.level = 2;}
		else if (this.score >= 3000 && this.score < 4000) {this.level = 3;}
		else if (this.score >= 4000 && this.score < 5000) {this.level = 4;}
		else if (this.score >= 5000 && this.score < 6000) {this.level = 5;}
		else if (this.score >= 6000 && this.score < 7000) {this.level = 6;}
		else if (this.score >= 7000 && this.score < 8000) {this.level = 7;}
		else if (this.score >= 8000 && this.score < 9000) {this.level = 8;}
		else {this.level = 9;}
	}
}
