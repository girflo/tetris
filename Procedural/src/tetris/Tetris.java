package tetris;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;  
class Tetris extends JPanel {
	private Color[][] matrix;
	private Info info;
	
	public static void main(String[] args) {
		Tetris tetris = new Tetris();
		tetris.setGameSettings();
		
		JFrame game = new JFrame("Tetris");
		game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.setSize(tetris.info.windowsWidth, tetris.info.windowsHeight);
		game.setVisible(true);
		game.add(tetris);
		game.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {
			}
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_LEFT:
					tetris.moveCurrentTetrominoLeft();
					break;
				case KeyEvent.VK_RIGHT:
					tetris.moveCurrentTetrominoRight();
					break;
				case KeyEvent.VK_DOWN:
					tetris.startAcceleration();
					break;
				case KeyEvent.VK_ALT:
					tetris.rotateCurrentTetrominoCW();
					break;
				case KeyEvent.VK_SHIFT:
					tetris.rotateCurrentTetrominoCCW();
					break;
				} 
			}
			
			public void keyReleased(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_DOWN:
					tetris.stopAcceleration();
					break;
				}
			}
		});
		tetris.play();
		
	}
	
	private void play() {
		do {
			moveCurrentTetrominoDown();
			try {
				Thread.sleep(gameSpeed());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} while (!info.gameFinish);
		repaint();
	}
	
	private void startAcceleration() {
		info.acceleration = 940 - (100 * info.level);
	}
	
	private void stopAcceleration() {
		info.acceleration = 0;
	}
	
	private int gameSpeed() {
		return 1000 - (100 * info.level) - info.acceleration;
	}
	
	private void setGameSettings() {
		createInfo();
		createMatrix();
	}
	
	private void createInfo() {
		this.info = new Info();
	}
	
	private void createMatrix() {
		this.matrix = new Color[10][20];
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 20; j++) {
				this.matrix[i][j] = Color.black;
			}
		}
	}
	
	@Override 
	public void paintComponent(Graphics g)
	{
		paintUI(g);
		paintMatrix(g);
		
		g.setFont(new Font("Arial", Font.PLAIN, 54));
		g.setColor(Color.white);
		paintScore(g);
		paintLevel(g);
		paintNextTetromino(g);
		paintCurrentTetromino(g);
		if (info.gameFinish == true) {
			paintLoosePanel(g);
		}
		
	}
	
	private void paintMatrix (Graphics g) {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 20; j++) {
				g.setColor(matrix[i][j]);
				g.fillRect(info.cellSize*i, info.cellSize*j, 78, 78);
			}
		}
	}
	
	private void paintUI(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, 800, 1600);
		g.fillRect(820, 0, 400, 600);
		g.fillRect(820, 620, 400, 400);
		g.fillRect(820, 1040, 400, 400);
	}
	
	private void paintScore(Graphics g) {
		g.drawString("Score", 920, 700);
		g.drawString(String.valueOf(info.score), 920, 800);
	}

	private void paintLevel(Graphics g) {
		g.drawString("Level", 920, 1120);
		g.drawString(String.valueOf(info.level), 920, 1220);
	}
	
	private void paintNextTetromino (Graphics g) {
		g.drawString("Next Tetromino", 840, 100);
		paintTetromino(g,info.nextTetromino, 8, 6);
	}
	
	private void paintCurrentTetromino (Graphics g) {
		paintTetromino(g,info.currentTetromino, 0, 0);
	}
	
	private void paintTetromino (Graphics g, Tetromino tetromino, int shiftX, int shiftY ) {
		g.setColor(tetromino.color);
		for (Point block : tetromino.position()) {
			g.fillRect((block.x + info.startCoordinates.x + shiftX) * info.cellSize, (block.y + info.startCoordinates.y + shiftY) * info.cellSize, 78, 78);
		}
	}
	
	private void paintLoosePanel(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(99, 299, 602, 302);
		g.setColor(Color.black);
		g.fillRect(100, 300, 600, 300);
		g.setColor(Color.white);
		g.setFont(new Font("Arial", Font.PLAIN, 80));
		g.drawString("Game Over", 200, 470);
	}
	
	private void rotateCurrentTetrominoCW(){
		boolean tetrominoCanBeMove = true;
		for (Point block : info.currentTetromino.blocks) {
			int blockCoordinateX = block.y + info.currentTetromino.movementX;
			int blockCoordinateY = (-block.x) + info.currentTetromino.movementY;
			if (!cellInsideMatrix(blockCoordinateX, blockCoordinateY) || !cellEmpty(blockCoordinateX, blockCoordinateY)){
				tetrominoCanBeMove = false;
				break;
			}
		}
		if (tetrominoCanBeMove == true) {
			info.currentTetromino.rotateCW();
			repaint();
		}
	}
	
	private void rotateCurrentTetrominoCCW(){
		boolean tetrominoCanBeMove = true;
		for (Point block : info.currentTetromino.blocks) {
			int blockCoordinateX = (-block.y) + info.currentTetromino.movementX;
			int blockCoordinateY = block.x + info.currentTetromino.movementY;
			if (!cellInsideMatrix(blockCoordinateX, blockCoordinateY) || !cellEmpty(blockCoordinateX, blockCoordinateY)){
				tetrominoCanBeMove = false;
				break;
			}
		}
		if (tetrominoCanBeMove == true) {
			info.currentTetromino.rotateCCW();
			repaint();
		}
	}
	
	private void moveCurrentTetrominoRight(){
		boolean tetrominoCanBeMove = true;
		for (Point block : info.currentTetromino.position()) {
			int blockCoordinateX = block.x;
			int blockCoordinateY = block.y;
			blockCoordinateX ++;
			if (!cellInsideMatrix(blockCoordinateX, blockCoordinateY) || !cellEmpty(blockCoordinateX, blockCoordinateY)){
				tetrominoCanBeMove = false;
				break;
			}
		}
		if (tetrominoCanBeMove == true) {
			info.currentTetromino.moveRight();
			repaint();
		}
	}
	
	public void moveCurrentTetrominoLeft(){
		boolean tetrominoCanBeMove = true;
		for (Point block : info.currentTetromino.position()) {
			int blockCoordinateX = block.x;
			int blockCoordinateY = block.y;
			blockCoordinateX --;
			if (!cellInsideMatrix(blockCoordinateX, blockCoordinateY) || !cellEmpty(blockCoordinateX, blockCoordinateY)){
				tetrominoCanBeMove = false;
				break;
			}
		}
		if (tetrominoCanBeMove == true) {
			info.currentTetromino.moveLeft();
			repaint();
		}
	}
	
	private void moveCurrentTetrominoDown() {
		boolean tetrominoCanBeMove = true;
		for (Point block : info.currentTetromino.position()) {
			int blockCoordinateX = block.x;
			int blockCoordinateY = block.y;
			blockCoordinateY ++;
			if (!cellInsideMatrix(blockCoordinateX, blockCoordinateY) || !cellEmpty(blockCoordinateX, blockCoordinateY)) {
				tetrominoCanBeMove = false;
				break;
			}
		}
		if (tetrominoCanBeMove == true) {
			info.currentTetromino.moveDown();
		}
		else {
			newTetromino();
		}
		repaint();
	}
	
	private boolean cellInsideMatrix(int cellCoordinateX, int cellCoordinateY) {
		if ((cellCoordinateX + info.startCoordinates.x > 9 || cellCoordinateX + info.startCoordinates.x < 0) || (cellCoordinateY + info.startCoordinates.y > 19)) {
			return false;
		}
		else return true;
	}
	
	private boolean cellEmpty(int cellCoordinateX, int cellCoordinateY) {
		int startCoodinateY = (cellCoordinateY + info.startCoordinates.y < 0 ? 0 : cellCoordinateY + info.startCoordinates.y);
		boolean cellState = (matrix[cellCoordinateX + info.startCoordinates.x][startCoodinateY] == Color.black) ? true : false;
		return cellState;
	}
	
	private void newTetromino() {
		addCurrentTetrominoToMatrix();
		info.newTetromino(numberOfFullLine());
		repaint();
	}
	
	private void addCurrentTetrominoToMatrix() {
		for (Point block : info.currentTetromino.position()) {
			if ((block.y + info.startCoordinates.y) < 0) {info.gameFinish = true;}
			else {matrix[block.x + info.startCoordinates.x][block.y + info.startCoordinates.y] = info.currentTetromino.color;}
		}
	}
	
	private int numberOfFullLine() {
		int fullLines = 0;
		List<Integer> fullLineIndexes = new ArrayList<Integer>();
		for (int i = 0; i < 20; i++) {
			boolean isFullLine = true;
			for (int j = 0; j < 10; j++) { 
				if (this.matrix[j][i] == Color.black) {
					isFullLine = false;
				}
			}
			if (isFullLine == true) {
				for (int j = 0; j < 10; j++) { 
					this.matrix[j][i] = Color.black;
				}
				fullLineIndexes.add(i);
				fullLines ++;
			}
		}
		if (fullLines > 0) {
			dropLines(fullLineIndexes);
		}
		return fullLines;
	}
	
	private void dropLines(List<Integer> fullLineIndexes){
		for (Integer index : fullLineIndexes){
			for (int i = 19; i > 1; i--) {
				if (i <= index) {
					for (int j = 0; j < 10; j++) { 
						this.matrix[j][i] = this.matrix[j][i-1];
					}
				}
			}
		}
		repaint();
	}

	
}