package tetris;
import java.awt.Color;
import java.awt.Point;
import java.util.concurrent.ThreadLocalRandom;

public class Tetromino {
	Color color;
	Point[] blocks;	
	int movementX;
	int movementY;
	
	public void tetrominoI(){
		this.color =  Color.red;
		this.blocks = new Point[]{ new Point(0, 0), new Point(0, 1), new Point(0, 2), new Point(0, (-1)) };
	}
	
	public void tetrominoJ(){
		this.color =  Color.yellow;
		this.blocks = new Point[]{ new Point(0, 0), new Point(0, (-1)), new Point(0, (-2)), new Point((-1), 0) };
	}
	
	public void tetrominoL(){
		this.color =  Color.green;
		this.blocks = new Point[]{ new Point(0, 0), new Point(0, (-1)), new Point(0, (-2)), new Point(1, 0) };
	}
	
	public void tetrominoO(){
		this.color =  Color.pink;
		this.blocks = new Point[]{ new Point(0, 0), new Point(0, 1), new Point((-1), 0), new Point((-1), 1) };
	}
	
	public void tetrominoS(){
		this.color =  Color.cyan;
		this.blocks = new Point[]{ new Point(0, 0), new Point(0, (-1)), new Point((-1), 0), new Point((-1), 1) };
	}
	
	public void tetrominoT(){
		this.color =  Color.orange;
		this.blocks = new Point[]{ new Point(0, 0), new Point(0, (-1)), new Point(0, 1), new Point(1, 0) };
	}
	
	public void tetrominoZ(){
		this.color =  Color.blue;
		this.blocks = new Point[]{ new Point(0, 0), new Point(0, 1), new Point((-1), 0), new Point((-1), (-1)) };
	}

	public Tetromino() {
		super();
		this.movementX = this.movementY = 0;
		int random = ThreadLocalRandom.current().nextInt(1, 7 + 1);
		if (random == 1) {tetrominoI();}
		else if (random == 2) {tetrominoJ();}
		else if (random == 3) {tetrominoL();}
		else if (random == 4) {tetrominoO();}
		else if (random == 5) {tetrominoS();}
		else if (random == 6) {tetrominoT();}
		else {tetrominoZ();}
	}
	
	public void moveRight(){
		this.movementX ++;
	}
	
	public void moveLeft(){
		this.movementX --;
	}
	
	public void moveDown(){
		this.movementY ++;
	}
	
	public void rotateCW(){
		int i = 0;
		Point[] newBlocks = new Point[4];
		for (Point block : blocks) {
			newBlocks[i] = new Point(block.y, (-block.x));
			i++;
		}
		this.blocks = newBlocks;
	}
	
	public void rotateCCW(){
		int i = 0;
		Point[] newBlocks = new Point[4];
		for (Point block : blocks) {
			newBlocks[i] = new Point((-block.y), block.x);
			i++;
		}
		this.blocks = newBlocks;
	}
	
	public Point[] position() {
		int i = 0;
		Point[] position = new Point[4];
		for (Point block : blocks) {
			position[i] = new Point((block.x + this.movementX), (block.y + this.movementY));
			i++;
		}
		return position;
	}
	
}