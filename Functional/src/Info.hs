-- This module define the game informations
module Info(Info(..), newGame, newTetromino, fixedBlocksCoodinates) where
import Tetromino
import Block
import Data.Bool
import Data.List

-- game info structure
data Info = Info
    -- current spent time
    { time :: Float
    -- current spent time since the last fixed block
    , fall :: Float
    -- current fall speed
    , speed :: Float
    -- game acceleration (when the user use the down key)
    , acceleration :: Float
    -- current game level
    , level :: Int
    -- position of a block when created
    , startTetrominoX :: Int
    , startTetrominoY :: Int
    -- position of the current tetromino
    , currentTetrominoX :: Int
    , currentTetrominoY :: Int
    -- the current tetromino
    , currentTetromino :: Tetromino
    -- the next tetromino
    , nextTetromino :: Tetromino
    -- contain all past tetromino that are now fixed to the matrix
    , fixedBlocks :: [Block]
    -- size of a cell (used by gloss in the UI module)
    , cellSize :: Int
    -- current score of the user
    , userScore :: Int
    -- tell if the game is over or not (used by the UI module in order to print the popup)
    , gameOver :: Bool
    } deriving (Show)

-- Set the default value of the game info
newGame :: Info
newGame = Info
    { time = 0
    , fall = 0
    , speed = 0.1
    , acceleration = 0
    , level = 0
    , startTetrominoX = (-240)
    , startTetrominoY = 840
    , currentTetrominoX = 0
    , currentTetrominoY = 0
    , currentTetromino = tetrominoL
    , nextTetromino = tetrominoZ
    , fixedBlocks = []
    , cellSize = 80
    , userScore = 0
    , gameOver = False
    }

-- Set the info for whenever a new tetromino is created
newTetromino :: Info -> Info
newTetromino info =
  do
    let newInfo = (removeFullLine info)
    newInfo { currentTetrominoX = 0
         , currentTetrominoY = 0
         , currentTetromino = (nextTetromino info)
         , nextTetromino = notRandomTetrominoWithTime}
    where
      notRandomTetrominoWithTime = notRandomTetromino (time info)

-- Check if the fixed block contain one or several full lines and remove them if needed
removeFullLine :: Info -> Info
removeFullLine info =
  if notElem True (map (\x -> checkForFullLine (x, (fixedBlocksCoodinates info))) [0..20])
    then info
    else info { userScore = ((userScore info) + newScore)
              , fixedBlocks = newfixedBlocks
              , level = newLevel}
          where
            -- List of boolean, for each line is true when the line is empty otherwise is false
            fullLinesBool = map (\x -> checkForFullLine (x, (fixedBlocksCoodinates info))) [0..20]
            -- List of indices of the empty lines
            indicesFullLines = elemIndices True fullLinesBool
            -- Add the score of the destroyed full lines to the current user score
            newScore = setScoreDependingOnFullLines (length (filter (\x -> x == True) fullLinesBool))
            -- Temporary fixed blocks, the full lines have been destroyed but the other lines need to be droped
            tmpFixedBlocks = removeFixedBlockOfFullLines indicesFullLines (fixedBlocks info)
            -- Drop all lines of the tmpFixedBlocks
            newfixedBlocks = dropOtherLines tmpFixedBlocks indicesFullLines
            -- Set the current level according to the new score
            newLevel = setLevelAccordingToScore ((userScore info) + newScore)

-- Remove all block from the fixed one if the Y coordinate is the same as the given one
removeFixedBlockOfFullLines :: [Int] -> [Block] -> [Block]
removeFixedBlockOfFullLines indexes fixedBlocks =
  filter (\x -> notElem (snd (blockCoordinates x)) indexes) fixedBlocks

-- Drop all fixed blocks by the given value
dropOtherLines :: [Block] -> [Int] -> [Block]
dropOtherLines fixedBloks indexes =
  map (\x -> dropBlock x (checkIfblockNeedToDrop (x, indexes))) fixedBloks

-- Check if the given block has one or several empty lines under it
checkIfblockNeedToDrop :: (Block, [Int]) -> Int
checkIfblockNeedToDrop (block, indexesFullLines) =
  sum (map f indexesFullLines)
  where
    f x = if (snd (blockCoordinates block)) < x
            then 1
            else 0

-- Return the score for the given number of destroyed lines
setScoreDependingOnFullLines :: Int -> Int
setScoreDependingOnFullLines fullLines =
  case fullLines of
  1 -> 40
  2 -> 100
  3 -> 300
  4 -> 1200

-- Return the level for the given user score
setLevelAccordingToScore :: Int -> Int
setLevelAccordingToScore score
  |  score >= 0 && score <= 1000 = 0
  |  score >= 1000 && score < 2000 = 1
  |  score >= 2000 && score < 3000 = 2
  |  score >= 3000 && score < 4000 = 3
  |  score >= 4000 && score < 5000 = 4
  |  score >= 5000 && score < 6000 = 5
  |  score >= 6000 && score < 7000 = 6
  |  score >= 7000 && score < 8000 = 7
  |  score >= 8000 && score < 9000 = 8
  |  otherwise = 9

-- Check if the fixed block contain 10 element with the given Y coordinate
checkForFullLine :: (Int, [(Int, Int)]) -> Bool
checkForFullLine (y, fixedBlocks) =
  do
    let currentLine = filter (\x -> snd x == y) fixedBlocks
    if (length currentLine) == 10
      then True
      else False

-- Return all the coordinates of the fixed blocks
fixedBlocksCoodinates :: Info -> [(Int, Int)]
fixedBlocksCoodinates info =
  map (\x -> blockCoordinates x ) (fixedBlocks info)
