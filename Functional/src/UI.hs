-- This module handle the everything linked to the game interface
module UI(displayGame) where
import Graphics.Gloss
import Tetromino
import Block
import Info

-- Draw the UI of the game
displayGame :: Info -> Picture
displayGame gameInfo =
  if ((gameOver gameInfo) == False)
    then Pictures [grid, nextPieceUi, scoreUi, wall, currentPiece, currentLevel]
    else Pictures [grid, nextPieceUi, scoreUi, wall, currentPiece, currentLevel, endGame]
  where
    grid =  Pictures [ Translate (-200) 0  matrix ]
    nextPieceUi = Pictures [ Translate 420 500 nextPiece, Translate 420 350 (displayTetromino (nextTetromino gameInfo))]
    scoreUi = Pictures [Translate 420 (-20) (score (userScore gameInfo))]
    wall = Pictures [Translate (fromIntegral (startTetrominoX gameInfo)) (fromIntegral (startTetrominoY gameInfo)) (displayFixedBlocks (fixedBlocks gameInfo))]
    currentPiece =  Pictures [ Translate (fromIntegral ((startTetrominoX gameInfo)+((cellSize gameInfo)*(currentTetrominoX gameInfo)))) (fromIntegral ((startTetrominoY gameInfo)-((cellSize gameInfo)*(currentTetrominoY gameInfo)))) (displayTetromino (currentTetromino gameInfo)) ]
    currentLevel = Pictures [Translate 420 (-440) (showLevel (level gameInfo))]
    endGame = Pictures [Translate 0 0 gameOverPopUp]

-- Level panel of the UI
showLevel :: Int -> Picture
showLevel level =
  Pictures
  [ Color black   (rectangleSolid 400 400)
  , Color white  (rectangleWire  400 400)
  , Translate (-90) 120 (printText "Level")
  , Translate (-170) 0 (printText (show level))]

-- The main panel of the UI where the game is playing
matrix :: Picture
matrix =
  Pictures
  [ Color black   (rectangleSolid 800 1600)
   , Color white  (rectangleWire  800 1600) ]

-- The next piece panel of the UI
nextPiece :: Picture
nextPiece =
  Pictures
  [ Color black   (rectangleSolid 400 600)
  , Color white  (rectangleWire  400 600)
  , Translate (-170) 220 (printText "Next piece") ]

-- The score panel of the UI
score :: Int -> Picture
score currentScore =
  Pictures
  [ Color black   (rectangleSolid 400 400)
  , Color white  (rectangleWire  400 400)
  , Translate (-90) 120 (printText "Score")
  , Translate (-170) 0 (printText (show currentScore))]

-- The game over pop up of the UI
gameOverPopUp :: Picture
gameOverPopUp =
  Pictures
  [ Color black   (rectangleSolid 600 300)
  , Color white  (rectangleWire  600 300)
  , Translate (-170) 0 (printText "Game Over")]

-- Print the content of the given string
printText :: String -> Picture
printText to_print =
  Pictures
  [ Scale 0.5 0.5
  $ Color white
  $ Text to_print  ]

-- Print all fixed block above the grid
displayFixedBlocks :: [Block] -> Picture
displayFixedBlocks blocks =
  Pictures (map (\x -> displayBlock ((blockCoordinates x), (blockColor x)) )  blocks)

-- Print the given tetromino
displayTetromino :: Tetromino -> Picture
displayTetromino tetromino =
  Pictures (map (\x -> displayBlock (x, tetrominoColor tetromino) ) (tetrominoBlocksCoordinates tetromino))

-- Print a block according to the given coordinates and color
displayBlock :: ((Int, Int), Color) -> Picture
displayBlock ((x, y), colour) =
  Pictures
  [ Translate ((fromIntegral x) * 80) ((fromIntegral y) * (-80)) (Color colour (rectangleSolid 80 80))
  , Translate ((fromIntegral x) * 80) ((fromIntegral y) * (-80)) (Color white (rectangleWire 80 80)) ]
