-- The main module of the game
module Main(main) where

import Graphics.Gloss

import UI
import Info
import Handler

-- Define the game windows information : name and size
window :: Display
window = InWindow "Tetris" (1280, 768) (200, 200)

-- Start the game
main :: IO ()
main = do
  -- play -> gloss's function for game
  -- windows -> the windows information
  -- (greyN 0.5) -> the background color
  -- 60 -> number of frame per second
  -- newGame (module Info) -> create the default values for the game info
  -- displayGame (module UI) -> print the components of the game
  -- userKey (module Handler) -> handle the user's input
  -- updateGameInfo (module Handler) -> update the state of the game according to the spent time
  play window (greyN 0.5) 60 newGame displayGame userKey updateGameInfo
