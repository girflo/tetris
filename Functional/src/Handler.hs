-- The module is the brain of the game, it contain the function that will change the content of the game info
-- depending of the user's game
module Handler(updateGameInfo, userKey) where
import Info
import Block
import Tetromino
import Data.Bool
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

-- Handle the user input key
userKey :: Event -> Info -> Info
-- left arrow -> move current tetromino left
userKey (EventKey (SpecialKey KeyLeft) Down _ _) info = moveTetrominoLeft info
-- right arrow -> move current tetromino right
userKey (EventKey (SpecialKey KeyRight) Down _ _) info = moveTetrominoRight info
-- down arrow (hold down) -> reduce the current tetromino falling time
userKey (EventKey (SpecialKey KeyDown) Down _ _) info = changeAcceleration 0.9 info
-- down arrow up -> set the falling time to the default value
userKey (EventKey (SpecialKey KeyDown) Up _ _) info = changeAcceleration 0 info
-- Left Crtl -> rotate the current tetromino (clockwise)
userKey (EventKey (SpecialKey KeyCtrlL) Down _ _) info = rotateCurrentTetrominoCW info
-- Left Alt -> rotate the current tetromino (counter clockwise)
userKey (EventKey (SpecialKey KeyAltL) Down _ _) info = rotateCurrentTetrominoCCW info
-- When the user don't input any key do nothing
userKey _ info = info

updateGameInfo :: Float -> Info -> Info
updateGameInfo t info =
  if (fall info + t) >= (((speed info)*(9 - (fromIntegral (level info)))) - ((acceleration info)))
  then do
      let newInfo = (moveTetrominoDown 1 info)
      newInfo {fall = 0, time = (time info + t)}
  else info {fall = (fall info + t), time = (time info + t)}

-- Set the current acceleration with the given value
changeAcceleration :: Float -> Info -> Info
changeAcceleration newAcceleration info =
  info {acceleration = newAcceleration}

-- If the current tetromino can be moved to the right move it
moveTetrominoRight :: Info -> Info
moveTetrominoRight info =
  if (elem True ((exceedGridSide (5, info)) ++ checkForTakenSlot (info, (1,0))))
    then info
    else info {currentTetrominoX = newTetrominoX}
      where
          newTetrominoX = ((currentTetrominoX info) + 1)

-- If the current tetromino can be moved to the left move it
moveTetrominoLeft :: Info -> Info
moveTetrominoLeft info =
  if (elem True ((exceedGridSide ((-4), info)) ++ checkForTakenSlot (info, ((-1),0))))
    then info
    else info {currentTetrominoX = newTetrominoX}
      where
          newTetrominoX = ((currentTetrominoX info) + (-1))

-- If the current tetromino can be moved left move it, otherwise fix it
moveTetrominoDown :: Int -> Info -> Info
moveTetrominoDown move info =
  if (elem True (checkForTakenSlot (info, (0, 1))))
    then
      if overflowTop info
        then info {gameOver = True}
        else setNextTetromino info
    else info {currentTetrominoY = newTetrominoY}
    where
      newTetrominoY = ((currentTetrominoY info) + (fromIntegral move))

-- Check if the coordinates of all block of the current tetromino are above the top of the matrix
overflowTop :: Info -> Bool
overflowTop info =
  elem True (map (\x -> lessThanTopY x (currentTetrominoY info) ) (tetrominoBlocksCoordinates (currentTetromino info)))

-- Check if the given coordinates are above the top of the matrix
lessThanTopY :: (Int, Int) -> Int -> Bool
lessThanTopY (blockX, blockY) currentTetrominoY =
  if ((blockY + currentTetrominoY) < 0)
    then True
    else False

-- Rotate the current tetromino if it can be (clockwise)
rotateCurrentTetrominoCW :: Info -> Info
rotateCurrentTetrominoCW info =
  if (elem False (rotateCWSpotEmpty info))
  then info
  else info {currentTetromino = newTetromino}
  where
    newTetromino = rotateTetrominoCW (currentTetromino info)

-- Rotate the current tetromino if it can be (counter clockwise)
rotateCurrentTetrominoCCW :: Info -> Info
rotateCurrentTetrominoCCW info =
  if (elem False (rotateCCWSpotEmpty info))
  then info
  else info {currentTetromino = newTetromino}
  where
    newTetromino = rotateTetrominoCCW (currentTetromino info)

-- Return a list a bool for each block of the current tetromino, is true if the block can be rotate, false otherwise (clockwise)
rotateCWSpotEmpty :: Info -> [Bool]
rotateCWSpotEmpty info =
  map (\x -> emptySpot ((addTwoCoordinateTupple ((blockCoordinates(rotateBlockCW x), ((currentTetrominoX info),(currentTetrominoY info))))), info) ) (tetrominoBlocks (currentTetromino info))

-- Return a list a bool for each block of the current tetromino, is true if the block can be rotate, false otherwise (counter clockwise)
rotateCCWSpotEmpty :: Info -> [Bool]
rotateCCWSpotEmpty info =
  map (\x -> emptySpot ((addTwoCoordinateTupple ((blockCoordinates(rotateBlockCCW x), ((currentTetrominoX info),(currentTetrominoY info))))), info) ) (tetrominoBlocks (currentTetromino info))

-- Add the two given coordinates together
addTwoCoordinateTupple :: ((Int, Int), (Int, Int)) -> (Int, Int)
addTwoCoordinateTupple ((firstPX, firstPY), (secondPX, secondPY)) =
  ((firstPX + secondPX), (firstPY + secondPY))

-- Check if the all coordinate of the current tetromino + the given move is taken
checkForTakenSlot :: (Info, (Int, Int)) -> [Bool]
checkForTakenSlot (info, (moveX, moveY)) =
  map (\x -> takenSpots (x, (moveX, moveY), (fixedBlocksCoodinates info))) (currentTetrominoCoordinates info)

-- Check if the coordinates of each blocks of the current tetromino exceed the grid size
exceedGridSide :: (Int, Info) -> [Bool]
exceedGridSide (move, info) =
  map (\x -> nextXPositionBlockOverflow(x, (currentTetrominoX info), move)) (tetrominoBlocksCoordinates (currentTetromino info))

-- check if the coordinate + the current x position are more than the given value
nextXPositionBlockOverflow :: ((Int, Int), Int, Int) -> Bool
nextXPositionBlockOverflow ((x, y), currentX, value) =
  if value > 0
    then if ((x+1) + currentX) > value
      then True
      else False
    else if ((x-1) + currentX) < value
      then True
      else False

-- Fix the current tetromino and generate a new one
setNextTetromino :: Info -> Info
setNextTetromino info =
  do
    let newInfo = fixTetromino info
    newTetromino newInfo

-- Add the current tetromino to the fixedBlocks
fixTetromino :: Info -> Info
fixTetromino info =
  info {fixedBlocks = newfixedBlocks}
  where
    newfixedBlocks = (fixedBlocks info) ++ (currentTetrominoToBlocks info)

-- Change the current tetromino to a list of block
currentTetrominoToBlocks :: Info -> [Block]
currentTetrominoToBlocks info =
  map (\x -> newBlock (x, (tetrominoColor(currentTetromino info))) ) (currentTetrominoCoordinates info)

-- Return the starting coordinates of the current tetromino with the current X and Y move
currentTetrominoCoordinates :: Info -> [(Int, Int)]
currentTetrominoCoordinates info =
  map (\x -> addPositionToMove(x, ((currentTetrominoX info),(currentTetrominoY info)))) (tetrominoBlocksCoordinates (currentTetromino info))

-- Add the two given position together
addPositionToMove :: ((Int,Int), (Int, Int)) -> (Int, Int)
addPositionToMove ((startX, startY), (moveX, moveY)) =
  ((startX + moveX), (startY + moveY))

-- Check if the given coordinate + the given move is taken
takenSpots :: ((Int, Int), (Int, Int), [(Int, Int)]) -> Bool
takenSpots ((x, y), (moveX, moveY), fixedTetrominos) =
  if (y + moveY) > 20
    then True
    else elem ((x + moveX), (y + moveY)) fixedTetrominos

-- Check if the given position is inside the matrix and empty
emptySpot :: ((Int, Int), Info) -> Bool
emptySpot ((x, y), info)
  | x > 5 = False
  | x < -4 = False
  | y > 20 = False
  | elem (x, y) (fixedBlocksCoodinates info) = False
  | otherwise = True
