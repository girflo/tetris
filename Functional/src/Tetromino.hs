-- This module define a tetromino
module Tetromino
    (
    Tetromino
    , tetrominoI
    , tetrominoJ
    , tetrominoL
    , tetrominoO
    , tetrominoS
    , tetrominoT
    , tetrominoZ
    , tetrominoBlocks
    , tetrominoBlocksCoordinates
    , tetrominoColor
    , rotateTetrominoCW
    , rotateTetrominoCCW
    , notRandomTetromino
    ) where
import Graphics.Gloss
import Block

-- Tetromino structure
data Tetromino = TetrominoContent [Block] deriving (Show)

-- Create the seven different tetrominos
tetrominoI = TetrominoContent [redBlock (0, 0), redBlock (0, 1), redBlock (0, (-1)), redBlock (0, (-2))]
tetrominoJ = TetrominoContent [yellowBlock (0,0), yellowBlock (0, (-1)), yellowBlock (0, (-2)), yellowBlock ((-1), 0)]
tetrominoL = TetrominoContent [greenBlock (0,0), greenBlock (0, (-1)), greenBlock (0, (-2)), greenBlock (1, 0)]
tetrominoO = TetrominoContent [purpleBlock (0,0), purpleBlock (0, (-1)), purpleBlock (1, 0), purpleBlock (1, (-1))]
tetrominoS = TetrominoContent [cyanBlock (0,0), cyanBlock (1, 0), cyanBlock (0, (-1)), cyanBlock ((-1), (-1))]
tetrominoT = TetrominoContent [orangeBlock (0,0), orangeBlock (0, 1), orangeBlock (0, (-1)), orangeBlock (1, 0)]
tetrominoZ = TetrominoContent [blueBlock (0,0), blueBlock (0, (-1)), blueBlock (1, (-1)), blueBlock ((-1), 0)]

-- Return a list of all blocks of a given tetromino
tetrominoBlocks :: Tetromino -> [Block]
tetrominoBlocks (TetrominoContent blocks) =
  blocks


  -- Return a list of all coordinates of the blocks of a given tetromino
tetrominoBlocksCoordinates :: Tetromino -> [(Int, Int)]
tetrominoBlocksCoordinates (TetrominoContent blocks) =
  map(\x -> blockCoordinates x ) blocks

-- return the color of a given tetromino
tetrominoColor :: Tetromino -> Color
tetrominoColor (TetrominoContent blocks) =
  head (map(\x -> blockColor x) blocks)

-- Generate a fake random tetromino using the current spent from the begining of the game
notRandomTetromino :: Float -> Tetromino
notRandomTetromino time =
  [tetrominoI, tetrominoJ, tetrominoL, tetrominoO, tetrominoS, tetrominoT, tetrominoZ] !! ((truncate(time * 1000)) `mod` 7)

-- Return a new tetromino from the given one for which each blocks has been rotated (clockwise)
rotateTetrominoCW :: Tetromino -> Tetromino
rotateTetrominoCW (TetrominoContent blocks) =
  TetrominoContent (map(\x -> rotateBlockCW x) blocks)

  -- Return a new tetromino from the given one for which each blocks has been rotated (counter clockwise)
rotateTetrominoCCW :: Tetromino -> Tetromino
rotateTetrominoCCW (TetrominoContent blocks) =
  TetrominoContent (map(\x -> rotateBlockCCW x) blocks)
