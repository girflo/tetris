-- This module define a block
-- A block is a square component of a tetromino
module Block
  (
  Block,
  rotateBlockCW,
  rotateBlockCCW,
  dropBlock,
  blockCoordinates,
  blockColor,
  newBlock,
  redBlock,
  yellowBlock,
  greenBlock,
  purpleBlock,
  cyanBlock,
  orangeBlock,
  blueBlock
  ) where
import Graphics.Gloss

-- Block structure
data Block = BlockContent ((Int, Int), Color) deriving (Show)

-- Change the coordinates of the block to make it rotated (clockwise)
rotateBlockCW :: Block -> Block
rotateBlockCW (BlockContent ((x, y), color)) =
  BlockContent ((y, -x), color)

  -- Change the coordinates of the block to make it rotated (counter clockwise)
rotateBlockCCW :: Block -> Block
rotateBlockCCW (BlockContent ((x, y), color)) =
  BlockContent ((-y, x), color)

-- Increase the Y coordinate of the block by the given value, be doing so the block go down the matrix
dropBlock :: Block -> Int -> Block
dropBlock (BlockContent ((x, y), color)) lineToDrop =
  BlockContent ((x, (y + lineToDrop)), color)

-- Return the coordinates of a given block
blockCoordinates :: Block -> (Int, Int)
blockCoordinates (BlockContent (coordinates, _)) =
  coordinates

-- Return the color of a given block
blockColor :: Block -> Color
blockColor (BlockContent (_, colour)) =
  colour

-- Create a new block with the given coordinates and color
newBlock :: ((Int, Int), Color) -> Block
newBlock ((x, y), color) =
  BlockContent ((x, y), color)

-- Create a new red block with the given coordinates
redBlock :: (Int, Int) -> Block
redBlock (x, y) =
  BlockContent ((x, y), red)

-- Create a new yellow block with the given coordinates
yellowBlock :: (Int, Int) -> Block
yellowBlock (x, y) =
  BlockContent ((x, y), yellow)

-- Create a new green block with the given coordinates
greenBlock :: (Int, Int) -> Block
greenBlock (x, y) =
  BlockContent ((x, y), green)

-- Create a new purple block with the given coordinates
purpleBlock :: (Int, Int) -> Block
purpleBlock (x, y) =
  BlockContent ((x, y), (mixColors 0.5 0.5 red blue))

-- Create a new cyan block with the given coordinates
cyanBlock :: (Int, Int) -> Block
cyanBlock (x, y) =
  BlockContent ((x, y), cyan)

-- Create a new orange block with the given coordinates
orangeBlock :: (Int, Int) -> Block
orangeBlock (x, y) =
  BlockContent ((x, y), orange)

-- Create a new blue block with the given coordinates
blueBlock :: (Int, Int) -> Block
blueBlock (x, y) =
  BlockContent ((x, y), blue)
